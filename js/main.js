
$(() => {
  let animationLength = 1000;
  let animationRunning = false;

  function switchCard(hide, show) {
    if (animationRunning) {
      return;
    }
    animationRunning = true;

    let $hide = $(hide);
    let $show = $(show);

    $hide.addClass("animationHideCard");

    setTimeout(() => {
      $hide.addClass("hidden");
      $hide.removeClass("animationHideCard");

      $show.removeClass("hidden");

      setTimeout(() => {
        $show.addClass("animationShowCard");

        setTimeout(() => {
          $show.removeClass("animationShowCard");

          animationRunning = false;
        }, animationLength);
      }, 1);
    }, animationLength);
  }

  $("section.front .flipper").click(() => {
    switchCard('section.front', 'section.back');
  });
  $("section.back .flipper").click(() => {
    switchCard('section.back', 'section.front');
  });

});
